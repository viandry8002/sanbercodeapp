import React,{useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Tugas1 from "./src/screens/Tugas1/Tugas1";
import Tugas2 from "./src/screens/Tugas2";
import Tugas3 from "./src/screens/Tugas3/TodoList";
import Tugas4 from "./src/screens/Tugas4/index";
import Tugas6 from "./src/screens/Tugas6/routes";
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';

var firebaseConfig = {
  apiKey: "AIzaSyDNftIy9H1QsHPefsMjS1kBhKI665BFAZ8",
  authDomain: "sanbercode-470c5.firebaseapp.com",
  databaseURL: "https://sanbercode-470c5.firebaseio.com",
  projectId: "sanbercode-470c5",
  storageBucket: "sanbercode-470c5.appspot.com",
  messagingSenderId: "399067462406",
  appId: "1:399067462406:web:d57c82c74f65dbb3b47c87",
  measurementId: "G-GC2K61T5JE"
};

// Initialize Firebase
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}


const App = () => {

  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init("d9e0cda9-e447-4f7c-9901-f2516059dedc", {kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption:2});
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received',onReceived)
    OneSignal.addEventListener('opened',onOpened)
    OneSignal.addEventListener('ids',onIds)
    
    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE
    },SyncStatus)
    
    return () => {
      OneSignal.removeEventListener('received',onReceived)
      OneSignal.removeEventListener('opened',onOpened)
      OneSignal.removeEventListener('ids',onIds)
    }
  }, [])

    const SyncStatus = (status) => {
      switch (status) {
        case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
          console.log("checking for update")
          break;
          case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
            console.log("Downloading package")
          break;
          case CodePush.SyncStatus.INSTALLING_UPDATE:
            console.log("installing update")
          break;
          case CodePush.SyncStatus.UPDATE_INSTALLED:
            console.log("Notification", "Update Installed")
          break;
          case CodePush.SyncStatus.AWAITING_USER_aCTION:
            console.log("Awaiting user")
          break;
        default:
          break;
      }
    }

    const onReceived = (notification) => {
      console.log("onReceived -> notofication",notification)
    }

    const onOpened = (openResult) => {
      console.log("onOpened -> openResult",openResult)
    }

    const onIds = (device) => {
      console.log("onIds -> device",device)
    }

  return (
    <Tugas6/>
  );
};

const styles = StyleSheet.create({
});

export default App;
