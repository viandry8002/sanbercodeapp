import React,{useEffect,useState} from 'react'
import { StyleSheet, 
  Text, 
  View, 
  StatusBar,
  Image, 
  SafeAreaView,
TouchableOpacity,
AppRegistry } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import foto from '../../assets/images/pken.jpeg';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from '../../style/StyleTugas2/styles.js'
import colors from '../../style/StyleTugas6/colors'
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../../api'
import { GoogleSignin } from '@react-native-community/google-signin';

// const Item = ({navigation}) => {
//   return(
//   <View>
//     <View style={styles.item}>
//     <AntDesign name={'setting'} size={30} />
//     <View style={styles.desc}>
//     <Text style={styles.title}>Pengaturan</Text>
//     </View>
//   </View>
//   <TouchableOpacity onPress={() => navigation.navigate('Bantuan')}>
//   <View style={styles.item} >
//     <Feather name={'help-circle'} size={30} />
//     <View style={styles.desc} >
//       {/* <TouchableOpacity  > */}
//     <Text style={styles.title}>Bantuan</Text>
//     {/* </TouchableOpacity> */}
//     </View>
//   </View>
//   </TouchableOpacity>
//   <View style={styles.item2}>
//             <SimpleLineIcons name={'book-open'} size={30} />
//     <View style={styles.desc}>
//     <Text style={styles.title}>{'Syarat & ketentuan'}</Text>
//     </View>
//   </View>
//   </View>
// )
// };

const Profile = ({navigation}) => {

    const [userInfo, setUserInfo] = useState(null)
    const [data, setData] = useState({})
    const [isGoogle, setIsGoogle] = useState(false)

    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("token")
                return getVenue(token)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
        getVenue()
        getCurentUser()
    }, [userInfo])

    const getCurentUser = async ()=>{
      try{
        const UserInfo = await GoogleSignin.signInSilently()
        if(UserInfo){
          // console.log("get Curent user -> userinfo",UserInfo)
          setUserInfo(UserInfo)
          setIsGoogle(true)
        }
      }catch(error){
        setIsGoogle(false)
      }

    }

    const getVenue = (token) =>{
        Axios.get(`${api}/profile/get-profile`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer' + token,
                'Accept' : 'application/json',
                'Content-Type' :  'application/json'
            }
        }).then((res) =>{
            // setNama(res.data.data.profile.name)
            // setFoto('https://crowdfunding.sanberdev.com' + res.data.data.profile.photo)
            setData(res.data.data.profile)
        }).catch((err) =>{
            console.log(err)
        })
    }

    const onLogout = async () => {
        try{
          if(userInfo !== null){
            await GoogleSignin.revokeAccess()
            await GoogleSignin.signOut()
          }
            await AsyncStorage.removeItem("token")
            // navigation.navigate('Login')

            navigation.reset({
              index : 0,
              routes : [{name : 'Login'}]
            })  

        }catch(err){
            console.log(err)
        }
    }

    const navigasiEdit = (route) => {
      navigation.navigate('EditProfile',route)
    }

    return (
        <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="#3598DB" />

      <TouchableOpacity onPress={() => navigasiEdit(data)}>
        <View style={styles.item1}  >
            {/* <Image source={foto? {uri : foto} : null} style={styles.avatar} /> */}
            <Image source={
              isGoogle == false ?
               data.photo == null ? 
                require('../../assets/images/blankp.png'):
                {
                  uri : 'https://crowdfunding.sanberdev.com' + data.photo,
                  cache : 'reload'
                } : {uri : userInfo && userInfo.user && userInfo.user.photo}
              } style={styles.avatar} />
            <View style={styles.desc}>
                {/* <Text style={styles.descNama}>{nama}</Text> */}
                <Text style={styles.descNama}>{
                isGoogle == false ?
                data.name :
                userInfo && userInfo.user && userInfo.user.name
                }</Text>
            </View>
        </View>
        </TouchableOpacity>

        <View style={styles.item2}>
                  <Ionicons name={'wallet-outline'} size={30} />
          <View style={styles.desc}>
            <Text style={styles.title}>Saldo</Text>
            <Text style={styles.saldo}>Rp. 120.000.000</Text>
          </View>
        </View>
        
        {/* <Item /> */}
  <View style={styles.item}>
    <AntDesign name={'setting'} size={30} />
    <View style={styles.desc}>
    <Text style={styles.title}>Pengaturan</Text>
    </View>
  </View>
  <TouchableOpacity onPress={() => navigation.navigate('Bantuan')}>
  <View style={styles.item} >
    <Feather name={'help-circle'} size={30} />
    <View style={styles.desc} >
      {/* <TouchableOpacity  > */}
    <Text style={styles.title}>Bantuan</Text>
    {/* </TouchableOpacity> */}
    </View>
  </View>
  </TouchableOpacity>
  <View style={styles.item2}>
            <SimpleLineIcons name={'book-open'} size={30} />
    <View style={styles.desc}>
    <Text style={styles.title}>{'Syarat & ketentuan'}</Text>
    </View>
  </View>
        
        <View style={styles.item} >
            <TouchableOpacity onPress={() => onLogout()}>
            <SimpleLineIcons name={'logout'} size={30} />
          {/* <View style={styles.descLogout} > */}
            <Text style={styles.title}>Keluar</Text>
          {/* </View> */}
          </TouchableOpacity>
        </View>

  </SafeAreaView>
    )
}

export default Profile

