import React,{useState,useEffect} from 'react'
import { View, Text,SafeAreaView,StatusBar,Button,TextInput,Alert } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import colors from '../../style/StyleTugas6/colors'
import styles from '../../style/StyleTugas6/styles'
import { Input } from 'react-native-elements';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../../api';
import { GoogleSignin, GoogleSigninButton,statusCodes } from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth'
import TouchID from 'react-native-touch-id';
import { TouchableOpacity } from 'react-native-gesture-handler';

const configFingerprint = {
    title: 'Authentication Required', 
    imageColor: '#e00606', 
    imageErrorColor: '#ff0000', 
    sensorDescription: 'Touch sensor', 
    sensorErrorDescription: 'Failed', 
    cancelText: 'Cancel'
}

const Login = ({ navigation }) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const saveToken = async (token) => {
        try {
            if(token !== null){
                await AsyncStorage.setItem('token', token)
            }
        } catch (e) {
          cosole.log(err)
        }
      }

    useEffect(() => {
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess : false,
            webClientId : '399067462406-3jmo35rmsnanmv6ubkepk7lsn0u61kvh.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try{
            await GoogleSignin.hasPlayServices();
            const {idToken} = await GoogleSignin.signIn()
            // console.log("signInWithGoogle -> idToken",idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken)
            
            auth().signInWithCredential(credential)

            // navigation.navigate('Profile')
            // navigation.navigate('MainTabNavigation')
            navigation.reset({
                index : 0,
                routes : [{name : 'MainTabNavigation'}]
            }) 
        }catch(error){
            console.log("signInWithGoogle -> error",error)
        }
    }

    const onLoginPress = () => {
        let data = {
            email : email,
            password : password
        }

        Axios.post(`${api}/auth/login`,data,{
            timeout: 20000
        }).then((res) => {
            // console.log("Login -> res",res.data)
            saveToken(res.data.data.token)
            // navigation.navigate('Profile')
            // navigation.navigate('MainTabNavigation')

            navigation.reset({
                index : 0,
                routes : [{name : 'MainTabNavigation'}]
            })            

        }).catch((err) => {
            Alert.alert('Peringatan',
                'Username atau Password salah',
                    [{
                        text : 'Ya',
                        onPress : () => console.log('Tidak')
                    }])
            console.log(err)
        })
    }

    const signInWithFingerptint = () =>{
        TouchID.authenticate('', configFingerprint)
        .then(success => {
            // Alert.alert('Authenticated Successfully');
            // navigation.navigate('Profile')
            navigation.navigate('MainTabNavigation')
        })
        .catch(error => {
            Alert.alert('Authentication Failed');
        });
    }

    return (
        <SafeAreaView style={styles.containerLogin}>
        <View style={styles.containerLogin}>
            <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
            <View style={styles.textLogoContainer}>
                <Text style={styles.textLogoLogin}>Login CrowdFunding</Text>
            </View>
            <View style={styles.inputLogin}>
            <Input value={email} placeholder='Nomor Ponsel atau Email'
            onChangeText={(email) => setEmail(email)}/>
            <Input secureTextEntry value={password} placeholder='Password'
            onChangeText={(password) => setPassword(password)}/>
            <Button title="LOGIN" onPress={() => onLoginPress()} />
                <View style={styles.textLoginDaftar}>
                    <Text>Belum Punya Akun? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Daftar')}>
                    <Text style={{color : colors.blue}} > Daftar</Text>
                    </TouchableOpacity>
                </View>
                <View>
                <GoogleSigninButton
                    // style={{ width: 192, height: 48 }}
                    style={styles.textLoginDaftar}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}
                    onPress={() => signInWithGoogle()}
                    // disabled={this.state.isSigninInProgress}
                     />
                <View style={styles.textLoginFinger}>
                <Button title="Sign in with Fingerprint" 
                onPress={() => signInWithFingerptint()}
                />
                </View>
                </View>
            </View>
        </View>
    </SafeAreaView>
    )
}

export default Login
