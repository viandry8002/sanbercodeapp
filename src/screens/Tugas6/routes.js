import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Intro from '../Tugas6/Intro';
import SplashScreens from '../Tugas6/Splashscreen';
import Login from '../Tugas7/Login';
import Profile from '../Tugas7/Profile';
import EditProfile from '../Tugas8/EditProfile';
import Daftar from '../MiniProject2/Daftar';
import DaftarOTP from '../MiniProject2/DaftarOTP';
import DaftarKataSandi from '../MiniProject2/DaftarKataSandi';
import HomePage from '../Tugas11/HomePage';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Feather from 'react-native-vector-icons/Feather';
import colors from '../../style/StyleTugas6/colors';
import Bantuan from "../Tugas12/Bantuan";
import Chart from '../Tugas13/Chart';
import Inbox from '../Tugas14/inbox';
import Riwayat from '../MiniProject3/Riwayat';
import BuatDonasi from '../MiniProject3/BuatDonasi';
import Donasi from '../Tugas11/Donasi';
import DetailDonasi from '../Tugas11/DetailDonasi';
import Payment from '../Tugas11/Payment';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Tab = createBottomTabNavigator();

const Stack = createStackNavigator()

function InboxStackScreen() {
  return (
    <Stack.Navigator>
     <Stack.Screen name="Inbox" component={Inbox} 
        options={{ headerShown: true ,
            title: 'Inbox',
            headerLeft: null,
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />
    </Stack.Navigator>
  );
}

function ProfileStackScreen() {
  return (
    <Stack.Navigator>
     <Stack.Screen name="Profile" component={Profile} 
        options={{ headerShown: true ,
            title: 'Account',
            headerLeft: null,
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />
    </Stack.Navigator>
  );
}

function HomeStackScreen() {
  return (
    <Stack.Navigator>
     <Stack.Screen name="HomePage" component={HomePage} 
        options={{ headerShown: false}} />
    </Stack.Navigator>
  );
}

const MainTabNavigation = () => {
  return(
      <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                  iconName = focused ? 'home' : 'home';
                } else if (route.name === 'Profile') {
                  iconName = focused ? 'user' : 'user';
                }else if (route.name === 'Inbox') {
                  iconName = focused ? 'inbox' : 'inbox';
                }

                // You can return any component that you like here!
                return <Feather name={iconName} size={size} color={color} />;
              },
            })}
            tabBarOptions={{
              activeTintColor: colors.blue,
              inactiveTintColor: colors.grey,
            }}
          >
            <Tab.Screen name="Home" component={HomeStackScreen} />
            <Tab.Screen name="Inbox" component={InboxStackScreen} />
            <Tab.Screen name="Profile" component={ProfileStackScreen} />
          </Tab.Navigator>
  )
}

const MainNavigation = (props) => (
    <Stack.Navigator initialRouteName={props.token !== null ? 'MainTabNavigation' : 'Intro'}> 
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: true,
         headerStyle: {
              backgroundColor: '#3598DB',
            },
            headerTintColor: '#fff' }} />
        
        <Stack.Screen name="Profile" component={Profile} 
        options={{ headerShown: true ,
            title: 'Account',
            headerLeft: null,
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />
        
        <Stack.Screen name="EditProfile" component={EditProfile} 
          options={{ headerShown: true ,
            title: 'Profil Saya',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />
        
        <Stack.Screen name="Daftar" component={Daftar} 
          options={{ headerShown: true ,
            title: 'Daftar',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />

        <Stack.Screen name="DaftarOTP" component={DaftarOTP} 
          options={{ headerShown: true ,
            title: 'Daftar',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />

        <Stack.Screen name="DaftarKataSandi" component={DaftarKataSandi} 
          options={{ headerShown: true ,
            title: 'Ubah Kata Sandi',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />

      <Stack.Screen name="MainTabNavigation" component={MainTabNavigation} options={{ headerShown: false }} />

      <Stack.Screen name="Bantuan" component={Bantuan} 
          options={{ headerShown: true ,
            title: 'Bantuan',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />

      <Stack.Screen name="Chart" component={Chart} 
          options={{ headerShown: true ,
            title: 'Statistik',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />

      <Stack.Screen name="Riwayat" component={Riwayat} 
          options={{ headerShown: true ,
            title: 'Riwayat',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />

      <Stack.Screen name="BuatDonasi" component={BuatDonasi} 
          options={{ headerShown: true ,
            title: 'Buat Donasi',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />

      <Stack.Screen name="Donasi" component={Donasi} 
          options={{ headerShown: true ,
            title: 'Donasi',
            headerStyle: {
                backgroundColor: '#3598DB',
              },
              headerTintColor: '#fff' 
            }} />

      <Stack.Screen name="DetailDonasi" component={DetailDonasi} 
          options={{ headerShown: true ,
            title: '',
            headerStyle: {
              backgroundColor: 'transparent',
              elevation: 0,
              shadowOpacity: 0,
              borderBottomWidth: 0,
              },
              headerTintColor: '#fff' 
            }} />

      <Stack.Screen name="Payment" component={Payment} 
          options={{ headerShown: true ,
            title: 'Payment',
            headerStyle: {
              backgroundColor: '#3598DB',
            },
              headerTintColor: '#fff' 
            }} />

    </Stack.Navigator>
)

const AppNavigation = () => {

    const [isLoading, setIsLoading] = useState(true)
    const [token, setToken] = useState(null)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

        async function getToken(){
          const token = await AsyncStorage.getItem('token')
          setToken(token)
        }
        getToken()

    },[])

    if(isLoading) {
        return <SplashScreens />
    }

    return (
        <NavigationContainer>
            <MainNavigation token={token}/>
        </NavigationContainer>
    )
}

export default AppNavigation