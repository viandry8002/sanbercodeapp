import React from 'react'
import { StyleSheet, 
  Text, 
  View, 
  StatusBar,
  Image, 
  SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import foto from '../../assets/images/pken.jpeg';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from '../../style/StyleTugas2/styles.js'

const Item = () => {
  return(
  <View>
    <View style={styles.item}>
    <AntDesign name={'setting'} size={30} />
    <View style={styles.desc}>
    <Text style={styles.title}>Pengaturan</Text>
    </View>
  </View>
  <View style={styles.item}>
    <Feather name={'help-circle'} size={30} />
    <View style={styles.desc}>
    <Text style={styles.title}>Bantuan</Text>
    </View>
  </View>
  <View style={styles.item2}>
            <SimpleLineIcons name={'book-open'} size={30} />
    <View style={styles.desc}>
    <Text style={styles.title}>{'Syarat & ketentuan'}</Text>
    </View>
  </View>
  </View>
)
};

function HomeScreen() {
  return (
    <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="#3598DB" />

        <View style={styles.item1}>
            <Image source={foto}
            style={styles.avatar} />
            <View style={styles.desc}>
                <Text style={styles.descNama}>Viandry Prasiswandava</Text>
            </View>
        </View>

        <View style={styles.item2}>
                  <Ionicons name={'wallet-outline'} size={30} />
          <View style={styles.desc}>
            <Text style={styles.title}>Saldo</Text>
            <Text style={styles.saldo}>Rp. 120.000.000</Text>
          </View>
        </View>
        
        <Item/>
        
        <View style={styles.item}>
<SimpleLineIcons name={'logout'} size={30} />
          <View style={styles.desc}>
            <Text style={styles.title}>Keluar</Text>
          </View>
        </View>

  </SafeAreaView>
  );
}

const Stack = createStackNavigator();
const index = () => {
    return (
        <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
        name="Account"
        component={HomeScreen}
        options={{
          title: 'Account',
          headerStyle: {
            backgroundColor: '#3598DB',
          },
          headerTintColor: '#fff',
        }}
      />
        </Stack.Navigator>
      </NavigationContainer>
    )
}

export default index

