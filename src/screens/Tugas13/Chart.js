import React,{useState} from 'react'
import { StyleSheet, Text, View,processColor } from 'react-native'
import colors from '../../style/StyleTugas6/colors'
import { BarChart } from 'react-native-charts-wrapper'
 
const data = [
    { y: 100 },
    { y: 60 },
    { y: 90 },
    { y: 45 },
    { y: 67 },
    { y: 32 },
    { y: 150 },
    { y: 70 },
    { y: 40 },
    { y: 89 },
]

const Chart = () => {

    const [legend, setLegend] = useState({
        enabled: false,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })
   
    const [chart, setChart] = useState({
        data: {
            dataSets: [{
              values: data,
              label: '',
              config: {
                color: processColor(colors.blue),
                // barShadowColor: processColor('lightblue'),
                stackLabels : [],
                drawFielled : false,
                drwaValues : false
              }
            }],
          },
    })

    const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
        
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        axisMaximum: new Date().getMonth() + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115,lineColor: processColor('red'),lineWidth: 1}],

        granularityEnabled: true,
          granularity : 1,
    })

    const [yAxis, setYAxis] = useState({
        left : {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right : {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false,
            enabled: false
        }
    })

    return (
            <View style={{flex: 1}}>
            <BarChart
                style={{flex: 1}}
                data={chart.data}
                xAxis={xAxis}
                yAxis={yAxis}
                animation={{durationX: 2000}}
                legend={legend}
                pinchZoom={false}
                doubleTapToZoomEnabled={false}
                chartDescription={{text:''}}
                marker={{
                    enabled: true,
                    textSize: 14
                }}
            />
            </View>
    )
}

export default Chart