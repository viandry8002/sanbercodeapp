import React,{useState, createContext} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import TodoList from './TodoList';

export const RootContext = createContext();

const index = () => {
    
    const [input, setInput] = useState('')
    const [todos, setTodos] = useState([])

    handleChangeInput = (value) => {
        setInput(value)
    }

    addTodo = () => {
        const day = new Date().getDate()
        const month = new Date().getMonth()
        const year = new Date().getFullYear()

        const today = `${day}/${month}/${year}`
        setTodos([...todos,{title:input,date: today}])
        setInput('')
    }

    removeTodo = (id) => {
        setTodos(
            todos.filter((todo,index)=>{
                if(index !== id){
                    return true
                }
            })
        )
    }

    return (
        <RootContext.Provider value={{
            input,
            todos,
            handleChangeInput,
            addTodo,
            removeTodo
        }}
        >
            <TodoList/>
        </RootContext.Provider>
    )
}

export default index