import React,{useState,useEffect,useContext} from 'react';
import Axios from 'axios';
import { StyleSheet, 
    Text, 
    View,
    TextInput,
    TouchableOpacity ,
    Alert,
    StatusBar,
    FlatList} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import { RootContext } from './index';
import styles from '../../style/StyleTugas4/styles'

const TodoList = () => {

    const state = useContext(RootContext)
    console.log("Todolist ->", state)
   
    const renderItem = ({item,index}) => {
        return (
            <View>
                <View style={styles.containerList}>
                    <View style={styles.textList}>
                        <Text>{item.date}</Text>
                        <Text>{item.title}</Text>
                    </View>
                    <View style={styles.deleteList}>
                        <TouchableOpacity onPress={() => state.removeTodo(index)}>
                            <FontAwesome name={'trash-o'} size={30} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    } 

    return (
        <View>
            <StatusBar barStyle="dark-content" backgroundColor="#fff" />
            <Text style={styles.judul} >Masukan TodoList</Text>
            <View style={styles.form}>
                <TextInput style={styles.input}
                placeholder="Input Here"
                value={state.input}
                onChangeText={(value) => state.handleChangeInput(value)}  />
                   <TouchableOpacity onPress={() => state.addTodo()} >
                    <View style={styles.button}>
                        <AntDesign name={'plus'}
                        size={20} />
                    </View>
                </TouchableOpacity>
            </View>
            <View>
                <FlatList
                    data={state.todos}
                    renderItem={renderItem}
                />
            </View>
        </View>
    )
}

export default TodoList
