import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View,StatusBar } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../../api'
import Axios from 'axios';
import colors from '../../style/StyleTugas6/colors'
import database from '@react-native-firebase/database';
import { GiftedChat } from 'react-native-gifted-chat'

const inbox = () => {

    const [user, setUser] = useState({})
    const [messages, setMessages] = useState([])

    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("token")
                if(token !== null){
                    return getVenue(token)
                }
            }catch(err){
                console.log(err)
            }
        }
        getToken()
        getVenue()
        onRef()

        return () => {
            const db = database().ref('messages')
            if(db){
                db.off()
            }
        }

    }, [])

    const getVenue = (token) =>{
        Axios.get(`${api}/profile/get-profile`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer' + token,
                'Accept' : 'application/json',
                'Content-Type' :  'application/json'
            }
        }).then((res) =>{
            setUser(res.data.data.profile)
        }).catch((err) =>{
            console.log(err)
        })
    }

    const onRef = () =>{
        database().ref('messages').limitToLast(20).on('child_added',snapshot => {
            const value = snapshot.val()
            setMessages(previousMessages => GiftedChat.append(previousMessages, value))
        })
    }

    const onSend = ((messages = []) => {
        for(let i = 0; i < messages.length; i++ ){
            database().ref('messages').push({
                _id : messages[i]._id,
                createdAt : database.ServerValue.TIMESTAMP,
                text : messages[i].text,
                user : messages[i].user
            })
        }
    })

    return (
        <View style={{flex: 1}}>
             <StatusBar barStyle="light-content" backgroundColor="#3598DB" />

            <GiftedChat 
            messages={messages}
            onSend={(messages) => onSend(messages)}
            user={{
                _id : user.id,
                name : user.name,
                avatar : 'https://crowdfunding.sanberdev.com' + user.photo
            }}
            showUserAvatar
            />

        </View>
    )
}

export default inbox

const styles = StyleSheet.create({})
