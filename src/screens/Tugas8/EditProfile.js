import React, { useState,useEffect,useRef } from 'react';
import { View, Text, Image, Alert, Modal, StatusBar, TextInput, TouchableOpacity,Button,ToastAndroid } from 'react-native';
// import styles from './style';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles'
import AsyncStorage from '@react-native-async-storage/async-storage';
import colors from '../../style/StyleTugas6/colors';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Axios from 'axios';
import api from '../../api'

function EditProfile({ navigation,route }) {

    const input = useRef(null)
    const camera = useRef(null)
    const [isVisible, setIsVisible] = useState(false)
    const [editable, setEditable] = useState(false)
    const [name, setName] = useState(route.params.name)
    const [email, setEmail] = useState(route.params.email)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)
    const [token, setToken] = useState('')

    const toggleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        const option = {quality: 0.5, base64 : true};
        if(camera){
            const data = await camera.current.takePictureAsync(option);
            setPhoto(data)
            setIsVisible(false)
        }
    }

    useEffect(() => {
        // console.log(route)
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("token")
                if(token !== null){
                    setToken(token)     
                }
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    }, [])

    const EditData = () => {
        setEditable(!editable)
    }

    const header = {
        'Authorization' : 'Bearer ' + token,
        'Accept' : 'application/json',
        'Content-Type' :  'multipart/form-data'
    }

    const onSavePress = () => {
        const formData = new FormData()
        formData.append('name',name)
        if(photo === null){
            formData.append('photo',route.params.photo)
        }else{
            formData.append('photo', {
                uri: photo.uri,
                name: 'photo.jpg',
                type: 'image/jpg'
            })
        }

        // const onLogout = async () => {
        //     try{
        //         await AsyncStorage.removeItem("token")
        //         navigation.navigate('Login')
    
        //     }catch(err){
        //         console.log(err)
        //     }
        // }
        
        Axios.post(`${api}/profile/update-profile`,formData,{
            timeout: 20000,
            headers : header 
        }).then((res) => {
        //    navigation.pop(1)
            if(res){
                ToastAndroid.showWithGravity(
                    'profile berhasil di update',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    0,
                    50
                )
            }
            EditData()
            navigation.navigate('MainTabNavigation')
            // Alert.alert('Peringatan',
            // 'Setelah terjadi perubahan, anda di minta untuk Logout Terlebih dahulu,apakah anda ingin keluar sekarang?',
            //     [{
            //         text : 'Tidak',
            //         onPress : () => console.log('Tidak')
            //     },{
            //         text : 'Ya',
            //         onPress : () => onLogout()
            //     }])

        }).catch((err) => {
            console.log(err)
        })
    }


    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={camera}
                    >
                        <View style={{
                             padding:25,
                             width:80,
                             height:80,
                             borderRadius:80,
                             backgroundColor:colors.blue
                        }}>
                            <TouchableOpacity style={{}} onPress={() => toggleCamera()}>
                                <MaterialCommunityIcons name={'rotate-3d-variant'} size={30} color={colors.white}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                              width:300,
                              height:500,
                              borderRadius:80,
                              borderColor:'black',
                              borderWidth:3,
                              borderColor: colors.black,
                              marginHorizontal: 50
                        }} />
                        <View style={{
                            padding:25,
                            width:80,
                            height:80,
                            borderRadius:80,
                            backgroundColor:colors.blue,
                            marginVertical: 30,
                            marginHorizontal: '40%'
                        }} >
                            <TouchableOpacity style={{}} onPress={() => takePicture()}>
                                <Feather name={'camera'} size={30} color={colors.white} />
                            </TouchableOpacity>
                        </View>

                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.inputLogin}>
                {/* <Image source={ photo === null ?{ uri : 'https://crowdfunding.sanberdev.com' + route.params.photo+ '?' + new Date(), cache: 'reload', headers: {Pragma: 'no-cache' }} : {uri : photo.uri}}
                style={{
                width:80,
                height:80,
                borderRadius:80,
                marginHorizontal:'40%'}} /> */}
                <Image source={ photo === null ?{ uri : 'https://crowdfunding.sanberdev.com' + route.params.photo} : {uri : photo.uri}}
                style={{
                width:80,
                height:80,
                borderRadius:80,
                marginHorizontal:'40%'}} />
                <TouchableOpacity onPress={() => setIsVisible(true)}>
                    <Icon name="camera" size={20} color={colors.white} 
                    style={{backgroundColor:colors.grey,
                    width:30,
                    height:30,
                    borderRadius:30,
                    padding:5,
                    marginHorizontal:'48%',
                    marginTop: -20
                    }} />
                </TouchableOpacity>
            </View>

            <View style={styles.inputLogin}>
                <Text style={styles.textEdit}>Nama Lengkap</Text>
                
                <View style={{flexDirection:'row'}} >
                <TextInput 
                ref={input}
                value={name}
                editable={editable}
                style={{width:330}}
                onChangeText={(name) => setName(name)}/>

                <SimpleLineIcons name={'pencil'} size={20} style={{paddingTop: 10}} onPress={() => EditData()} />
                
                </View>
                
                <Text style={styles.textEdit}>Email</Text>
                <TextInput value={email}  editable={false} color={'black'}/>
                <Button title="SIMPAN" 
                onPress={() => onSavePress()}
                />
            </View>
            {renderCamera()}
        </View>
    )
}

export default EditProfile;