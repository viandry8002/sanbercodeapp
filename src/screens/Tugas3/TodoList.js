import React,{useState,useEffect} from 'react';
import Axios from 'axios';
import { StyleSheet, 
    Text, 
    View,
    TextInput,
    TouchableOpacity ,
    Alert,
    StatusBar} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import styles from '../../style/StyleTugas3/styles'

const List = ({Judul,Tanggal,onDelete}) => {
    return (
        <View>
            <View style={styles.containerList}>
                <View style={styles.textList}>
                    <Text>{Tanggal}</Text>
                    <Text>{Judul}</Text>
                </View>
                <View style={styles.deleteList}>
                    <TouchableOpacity onPress={onDelete}>
                        <FontAwesome name={'trash-o'} size={30} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const TodoList = () => {
    const [Judul, setJudul] = useState("");
    const [ListData, setListData] = useState([]);
    const [Tanggal, setTanggal] = useState('');

    useEffect(() => {
        getData();
        var date = moment(new Date())
                  .format('DD/MM/YYYY');
        setTanggal(date);
    }, [])

    const getData = () =>{
        Axios.get('http://10.0.2.2:3004/list')
        .then(res => {
            console.log(res)
            setListData(res.data)
        })
    }

    const onInsert = () =>{

        const data = {
            Judul,
            Tanggal
        }

        Axios.post('http://10.0.2.2:3004/list',data)
        .then(res => {
                console.log(res)
                setJudul("")
                getData() 
            }
        )
    }

    const deleteList = (List) => {
        Axios.delete(`http://10.0.2.2:3004/list/${List.id}`)
        .then(res => {
            console.log(res)
            getData()
        })
    }

    return (
        <View>
            <StatusBar barStyle="dark-content" backgroundColor="#fff" />
            <Text style={styles.judul} >Masukan TodoList</Text>
            <View style={styles.form}>
                <TextInput style={styles.input} 
                placeholder="Input Here" value={Judul} onChangeText={(value) => setJudul(value)} />
                   <TouchableOpacity onPress={onInsert} >
                    <View style={styles.button}>
                        <AntDesign name={'plus'} size={20} />
                    </View>
                </TouchableOpacity>
            </View>
            {ListData.map(ListData => {
                return <List 
                    key={ListData.id}
                    Judul={ListData.Judul}
                    Tanggal={ListData.Tanggal}
                    onDelete = {() =>
                    Alert.alert('Peringatan',
                    'Apakah anda ingin menghapus List ini ?',
                    [{
                        text : 'Tidak',
                        onPress : () => console.log('Tidak')
                    },{
                        text : 'Ya',
                        onPress : () => deleteList(ListData)
                    }])
                    }
                />    
            })}

        </View>
    )
}

export default TodoList
