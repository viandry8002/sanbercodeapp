import React,{useEffect} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import MapboxGL from '@react-native-mapbox-gl/maps'
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Foundation from 'react-native-vector-icons/Foundation';

MapboxGL.setAccessToken('pk.eyJ1IjoidmlhbjIwOCIsImEiOiJja2hsbjRnZGUwMndiMzNtazRpc3ZrZ2tlIn0.IMZF3r8zV4Eq0CpywQXd3Q')

const coordinates = [
    [107.580110, -6.890066],
    [106.819449, -6.218465],
    [110.365231, -7.795766]
]

const Bantuan = () => {

    useEffect(() => {
        const getLocation = async() =>{
            try{
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            }catch(error){
                console.log(error);
            }
        }
        getLocation()
    })

    return (
        <View style={{flex : 1,flexDirection: 'column',marginBottom: 120}}>
            <MapboxGL.MapView style={{flex : 1,marginBottom: 10}}>
                
                <MapboxGL.UserLocation
                visible={true}
                />

                <MapboxGL.Camera
                // followUserLocation={true}
                centerCoordinate={coordinates[0]}
                zoomLevel={5}
                />
            {/* // point 1 */}
                {/* <MapboxGL.PointAnnotation
                id="pointAnnotation"
                coordinate={coordinates[0]}
                >
                <MapboxGL.Callout
                title={`Longtitude ${coordinates[0][0]} \n Latitude ${coordinates[0][1]}`}
                />
                </MapboxGL.PointAnnotation> */}
            
            {/* point 2 */}
                {/* <MapboxGL.PointAnnotation
                id="pointAnnotation"
                coordinate={coordinates[1]}
                >
                <MapboxGL.Callout
                title={`Longtitude ${coordinates[1][0]} \n Latitude ${coordinates[1][1]}`}
                />
                </MapboxGL.PointAnnotation> */}
            {/* point 3 */}
                {/* <MapboxGL.PointAnnotation
                id="pointAnnotation"
                coordinate={coordinates[2]}
                >
                <MapboxGL.Callout
                title={`Longtitude ${coordinates[2][0]} \n Latitude ${coordinates[2][1]}`}
                />
                </MapboxGL.PointAnnotation> */}

            {
                coordinates.map((item,index) => {
                    return (
                        <MapboxGL.PointAnnotation
                        key={index}
                        id={`pointAnnotation${index}`}
                        coordinate={item}
                        >
                        <MapboxGL.Callout
                        title={`Longtitude ${item[0]} \n Latitude ${item[1]}`}
                        />
                        </MapboxGL.PointAnnotation>
                    )
                })
               
            }
            


                </MapboxGL.MapView>

                <View style={styles.item}>
                <Ionicons name={'home'} size={30} />
                <View style={styles.desc}>
                <Text style={styles.title}>Jakarta,Bandung,Yogyakarta</Text>
                </View>
            </View>
            <View style={styles.item} >
                <Entypo name={'mail'} size={30} />
                <View style={styles.desc} >
                <Text style={styles.title}>customer_service@crowdfunding.com</Text>
                </View>
            </View>
            <View style={styles.item}>
                <Foundation name={'telephone'} size={30} />
                <View style={styles.desc}>
                <Text style={styles.title}>(021)777-888</Text>
                </View>
            </View>
        </View>
    )
}

export default Bantuan

const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        backgroundColor: "#fff",
        padding: 20,
        marginVertical: 1,
      },
      title: {
        fontSize: 15,
        padding: 5
      },
      desc: {marginLeft: 18, flex: 1, flexDirection: 'row'},
})
