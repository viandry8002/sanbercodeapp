import React,{useEffect,useState} from 'react'
import { StyleSheet, Text, View,SafeAreaView, FlatList, StatusBar,TouchableOpacity,Image } from 'react-native'
import colors from '../../style/StyleTugas6/colors'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../api'
import { TextMask } from 'react-native-masked-text'
import { ProgressBar, Colors } from 'react-native-paper';

const Donasi = ({ navigation }) => {
    const [donasi, setDonasi] = useState([])

    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("token")
                return getDonasi(token)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
        getDonasi()
    }, [])

    const navDetailDonasi = (route) => {
        navigation.navigate('DetailDonasi',route)
      }

    const Item = ({ data }) => (
        <TouchableOpacity onPress={() => navDetailDonasi(data)}>
        <View style={styles.item}>
        <Image source={data.photo === null ? require('../../assets/images/img-notfound.png') : {uri : 'https://crowdfunding.sanberdev.com' + data.photo}} style={{width: 200,height: 130,borderRadius:10, marginHorizontal:10}} />

        <View style={styles.containerflatList}>
          <Text style={styles.title1}>{data.title}</Text>
          <Text style={styles.title}>{data.user.name}</Text>
          <ProgressBar progress={0.5} color={Colors.red800} />
          <Text style={styles.title}>Dana yang dibutuhkan</Text>
          <TextMask
            type={'money'}
            options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp. ',
                suffixUnit: ''
            }}
            value={data.donation}
            style={{paddingTop:5,fontWeight:'bold'}}
            />
        </View>
        </View>
        </TouchableOpacity>
      );

    const getDonasi = (token) =>{
        Axios.get(`${api}/donasi/daftar-donasi`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer' + token,
                'Accept' : 'application/json',
                'Content-Type' :  'application/json'
            }
        }).then((res) =>{
            setDonasi(res.data.data.donasi)
        }).catch((err) =>{
            console.log(err)
        })
    }

    const renderItem = ({ item }) => (
        <Item data={item} />
      );
    
      return (
        <SafeAreaView style={styles.container}>
          <FlatList
            data={donasi}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
        </SafeAreaView>
      );
}

export default Donasi

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item: {
        backgroundColor: colors.white,
        padding: 10,
        marginVertical: 3,
        flexDirection : 'row',
        flex: 1
      },
      title1: {
        fontSize: 13,
        fontWeight: "bold"
      },
      title: {
        fontSize: 13,
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1
      }
})

