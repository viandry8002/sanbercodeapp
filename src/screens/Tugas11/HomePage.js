import React, { Component,useEffect,useState} from 'react'
import { StyleSheet, Text, View, Image, StatusBar, Dimensions,TextInput,FlatList } from 'react-native'
import Swiper from 'react-native-swiper'
const { width, height } = Dimensions.get('window')
import colors from '../../style/StyleTugas6/colors'
import AntDesign from 'react-native-vector-icons/AntDesign';
import { SliderBox } from "react-native-image-slider-box";
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { TextMask } from 'react-native-masked-text'
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../../api'


const styles = StyleSheet.create({
   
    container: {
      flex: 1,
      flexDirection: 'column'
    },
  
    wrap1: {
      width : 50,
      height: 50,
      backgroundColor: colors.blue
    },
    avatar: {
      width: 40,
      height: 40,
      borderRadius: 40
  },
  input: {
     borderWidth: 1,
     width: 220,
     borderRadius: 25,
     borderColor: colors.white,
     paddingHorizontal: 10,
     marginBottom: 70,
     color: colors.white,
    }
  })

const HomePage = ({ navigation }) => {

  const [data, setData] = useState({})

  useEffect(() => {
    async function getToken() {
        try{
            const token = await AsyncStorage.getItem("token")
            return getVenue(token)
        }catch(err){
            console.log(err)
        }
    }
    getToken()
    getVenue()
}, [])

  const getVenue = (token) =>{
    Axios.get(`${api}/profile/get-profile`,{
        timeout : 20000,
        headers : {
            'Authorization' : 'Bearer' + token,
            'Accept' : 'application/json',
            'Content-Type' :  'application/json'
        }
    }).then((res) =>{
        setData(res.data.data.profile)
    }).catch((err) =>{
        console.log(err)
    })
}

  const state = {
    images: [
      "https://source.unsplash.com/1024x768/?nature",
      "https://source.unsplash.com/1024x768/?water",
      "https://source.unsplash.com/1024x768/?girl",
      "https://source.unsplash.com/1024x768/?tree",
    ]
  };

  const datas = [
    {
      id:1,
      title:'Lorem ipsum dolor sit',
      total: 20000000,
      images: 'https://cms-assets.tutsplus.com/uploads/users/30/posts/27866/preview_image/pre.png'
    },
    {
      id:2,
      title:'Lorem ipsum dolor sit',
      total: 30000000,
      images: 'https://www.ajnn.net/files/images/20190114-desa-dana.jpg'
    },
    {
      id:3,
      title:'Lorem ipsum dolor sit',
      total: 30000000,
      images: 'https://img2.pngdownload.id/20190321/vcy/kisspng-vector-graphics-donation-portable-network-graphics-donate-png-vectors-vector-clipart-psd-peoplepn-5c933fd94efc86.6468555415531540093235.jpg'
    },
    {
      id:4,
      title:'Lorem ipsum dolor sit',
      total: 30000000,
      images: 'https://www.ajnn.net/files/images/20190114-desa-dana.jpg'
    },
  ]

  const renderItem = ({item}) => {
    return(
      <View style={{
        width: 250,
        height: 200,
        flexDirection: 'column',
        elevation: 5,
        backgroundColor: colors.white,
        borderRadius:20,
        margin:10,
      }} >
        <Image source={{uri : item.images}} style={{width:'100%',height:100,borderTopRightRadius:20,borderTopLeftRadius:20 }} />
        <View style={{padding: 10}} >
          <Text>{item.title}</Text>
          <TextMask
            type={'money'}
            options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp. ',
                suffixUnit: ''
            }}
            value={item.total}
            style={{paddingTop:5,fontWeight:'bold'}}
            />
        </View>
      </View>
    )
  }

    return (
      <ScrollView>
        <View style={styles.container}>
        <StatusBar backgroundColor={colors.blue} barStyle="light-content" />

        <View style={{width: '100%', height: 150, backgroundColor: colors.blue,padding: 20,flexDirection: 'row',justifyContent: 'space-between',}} >
            {/* <Image source={require('../../assets/images/blankp.png')} style={styles.avatar} /> */}
            <Image source={
               data.photo == null ? 
                require('../../assets/images/blankp.png'):
                {
                  uri : 'https://crowdfunding.sanberdev.com' + data.photo,
                  cache : 'reload'
                }
              } style={styles.avatar} />
            
            <TextInput placeholder="Cari disini" placeholderTextColor={colors.white} style={styles.input} />

            <AntDesign name={'hearto'} size={30} color={colors.white}/>
        </View>

        <View  style={{
          marginHorizontal: '5%',
          paddingHorizontal: 20,
          paddingVertical: 15,
          marginTop: 90,
          backgroundColor: colors.white,
          width: '90%',
          height: 100,
          position: 'absolute',
          flexDirection: 'row',
          borderRadius: 20,
          elevation: 5,
          zIndex: 1,
          justifyContent: 'space-between',}}>
            <TouchableOpacity>
              <View style={{backgroundColor: colors.blue,width: 90,height: 65,borderRadius: 10,padding: 5}}>
              <View style={{flexDirection: 'row'}}>
              <Image source={require('../../assets/icon/wallet.png')} style={{width: 30, height: 30}} />
              <Text style={{color:colors.white,padding:5}}>Saldo</Text>
              </View>
              <Text style={{color:colors.white,padding:3}}>Rp. 0</Text>
              </View>
            </TouchableOpacity>
           
            <TouchableOpacity style={{paddingTop: 10}}>
            <Image source={require('../../assets/icon/add.png')} style={{width: 30, height: 30}} />
            <Text style={{textAlign:'center'}}>Isi</Text> 
            </TouchableOpacity>

            <TouchableOpacity style={{paddingTop: 10}}>
            <Image source={require('../../assets/icon/reload.png')} style={{width: 30, height: 30}} />
            <Text style={{textAlign:'center'}}>Riwayat</Text> 
            </TouchableOpacity>
            
            <TouchableOpacity style={{paddingTop: 10}}>
            <Image source={require('../../assets/icon/menu.png')} style={{width: 30, height: 30}} />
            <Text style={{textAlign:'center'}}>Lainnya</Text> 
            </TouchableOpacity>
           
        </View> 

        <View style={{height: 350, 
          backgroundColor: colors.white,     
          paddingTop: 60,
          flexDirection: 'column'}} >

            
        <View>
        <SliderBox 
        autoplay
        circleloop
        images={state.images}
        SliderBoxHeight={200}
        dotColor={colors.blue}
        inactiveDotColor={colors.white}
        imageLoadingColor={colors.blue}
        ImageComponentStyle={{width: '92%',borderRadius: 5}} />
        </View>

        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 10,
          paddingHorizontal: '5%',
         }}>
           <TouchableOpacity onPress={() => navigation.navigate('Donasi')}>
            <Image source={require('../../assets/icon/donation.png')} style={{width: 50, height: 50}} />
            <Text>Donasi</Text> 
            </TouchableOpacity>
           
            <TouchableOpacity onPress={() => navigation.navigate('Chart')}>
            <Image source={require('../../assets/icon/stats.png')} style={{width: 50, height: 50}} />
            <Text>Statistik</Text> 
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('Riwayat')}>
            <Image source={require('../../assets/icon/clock.png')} style={{width: 50, height: 50}} />
            <Text>Riwayat</Text> 
            </TouchableOpacity>
            
            <TouchableOpacity onPress={() => navigation.navigate('BuatDonasi')}>
            <Image source={require('../../assets/icon/charity.png')} style={{width: 50, height: 50}} />
            <Text>Bantu</Text> 
            </TouchableOpacity>
        
        </View>
        </View>


        <View  style={{
          height:270,
          padding:15,
          marginTop: 10,
          flexDirection: "column",
          backgroundColor: colors.white,
        }}>
          <Text style={{fontSize:16,fontWeight:'bold'}}>Penggalangan Dana Mendesak</Text>
          <View style={{flex: 1}}>
            <FlatList
              data={datas}
              horizontal
              renderItem={renderItem}
              key={(item) => item.id}
              style={{marginLeft : -20}}
              keyExtractor={(item) => item.id.toString()}
              showsHorizontalScrollIndicator={false}
            />
          </View>
           
        </View> 
      
      </View>
      </ScrollView>
    )
}

export default HomePage

