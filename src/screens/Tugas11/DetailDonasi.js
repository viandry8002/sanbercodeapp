import React,{useEffect,useState} from 'react'
import { StyleSheet, Text, View,Image,Button,TouchableOpacity,Modal,TouchableHighlight,Alert,FlatList } from 'react-native'
import { TextMask } from 'react-native-masked-text'
import colors from '../../style/StyleTugas6/colors'
import { TextInputMask } from 'react-native-masked-text'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../api';

const NOMINAL = [
    {
      id: 'nominal50',
      total: 50000 ,
    },
    {
      id: 'nominal100',
      total: 100000 ,
    },
    {
      id: 'nominal200',
      total: 200000 ,
    }
  ];

const DetailDonasi = ({navigation,route}) => {
    const [data, setData] = useState(route.params)
    const [isVisible, setIsVisible] = useState(false)
    const [tokens, setTokens] = useState('')
    const [datas, setDatas] = useState({})
    const [donation, setDonation] = useState('')
    const [selected, setSelected] = useState(null)


    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("token")
                setTokens(token)
                return getVenue(token)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
        getVenue()
    }, [])

    const getVenue = (token) =>{
        Axios.get(`${api}/profile/get-profile`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer' + token,
                'Accept' : 'application/json',
                'Content-Type' :  'application/json'
            }
        }).then((res) =>{
            setDatas(res.data.data.profile)
        }).catch((err) =>{
            console.log(err)
        })
    }

    const header = {
        'Authorization' : 'Bearer ' + tokens,
        'Accept' : 'application/json',
        'Content-Type' :  'application/json'
    }

    const onTopUpPress = () => {
        const time = new Date().getTime()
        let donations =  donation.replace(/[^\w\s]/gi, '')
        let donations1 =  donations.replace('Rp', '')
        const body = {
            transaction_details : {
                order_id : `Donasi-${time}`,
                gross_amount : selected !== null ? selected : donations1,
                donation_id : time
            },
                customer_details : {
                first_name    : datas.name,
                email         : datas.email
            }
        }
        Axios.post(`${api}/donasi/generate-midtrans`,body,{
            timeout: 20000,
            headers : header 
        }).then((res) => {
            console.log("success -> res",res.data)
            const data = res.data.data
            navigation.navigate('Payment',data)
            setIsVisible(!isVisible)

        }).catch((err) => {
            console.log(err)
             Alert.alert('Peringatan',
            'Terjadi kesalahan pada transaksi,mohon di coba kembali',
                [{
                    text : 'Ya',
                    onPress : () => console.log('Ya')
                }])
        })
    }

    const renderDonation = () =>{
        return(
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>

                <Text style={styles.modalText}>ISI NOMINAL</Text>

                <TextInputMask
            type={'money'}
            options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp. ',
                suffixUnit: ''
            }}
            value={donation}
            onChangeText={(donation) => setDonation(donation)}
            placeholder={'Rp. 0'}
            />

            <FlatList
                data={NOMINAL}
                renderItem={renderItem}
                keyExtractor={item => item.id.toString()}
                horizontal
            />

            <View style={{flexDirection: 'row',justifyContent: 'space-around',margin:10}}>
            <Button
                title="LANJUT"
                onPress={() => onTopUpPress()}
                />
            <Button
                title={'kembali'}
                onPress={() => setIsVisible(!isVisible)}
                />
            </View>

              </View>
            </View>
          </Modal>
        )
    }

    const onSelect = (item) => {
        if (selected === null){
            setSelected(item.total)
        }else{
            setSelected(null)
        }
    }

    const Item = ({ item }) => {
    const bgColor = item.total == selected ? colors.blue : colors.white
    const Coloritm = item.total == selected ? colors.white : colors.black
    return (
            <View style={{
                height:40,
                backgroundColor: bgColor,
                elevation: 5,
                padding: 10,
                marginHorizontal: 5,
                marginVertical: 10
            }}>
                <TouchableOpacity onPress={() =>  onSelect(item)}>
                <TextMask
                type={'money'}
                options={{
                    precision: 0,
                    separator: '.',
                    delimiter: '.',
                    unit: 'Rp. ',
                    suffixUnit: ''
                }}
                value={item.total}
                style={{color:Coloritm}}
                />
                </TouchableOpacity>
            </View>
        );
    }

        const renderItem = ({ item }) => {
        return (
            <Item item={item} />
        )};

        
    return (
        <View style={{flex:1}}>
            <Image 
            source={data.photo === null ? require('../../assets/images/img-notfound.png') : {uri : 'https://crowdfunding.sanberdev.com' + data.photo}}
            style={{
                width: '100%',
                height: 300,
                marginTop:-70
            }}/>

            <View style={{padding: 15,flexDirection:'column',backgroundColor: colors.white,width: '100%',height:260,marginBottom:10}}>
            <Text style={{fontSize: 15,fontWeight: "bold",padding:3}} >{data.title}</Text>
                
                <View style={{flexDirection:'row',padding:3}}>
                    <Text>Dana yang dibutuhka</Text>
                    <TextMask
                        type={'money'}
                        options={{
                            precision: 0,
                            separator: '.',
                            delimiter: '.',
                            unit: 'Rp. ',
                            suffixUnit: ''
                        }}
                        value={data.donation}
                        />
                </View>
                
                <Text style={{fontSize: 15,fontWeight: "bold",padding:3}}>Deskripsi</Text>
                <Text style={{padding:3}}>{data.description}</Text>
                    
                <View style={{padding:5,marginTop:10}}>
                <Button
                title="DONASI SEKARANG"
                onPress={() => setIsVisible(true)}
                />
                
                </View>

            </View>

            <View style={{padding: 15,flexDirection:'column',backgroundColor: colors.white,width: '100%',height:200}}>
                <Text style={{fontSize: 15,fontWeight: "bold",padding:3}}>Informasi Penggalangan Dana</Text>

                <View  style={{
          marginHorizontal: '5%',
          paddingHorizontal: 20,
          paddingVertical: 10,
          marginTop: 10,
          backgroundColor: colors.white,
          width: '90%',
          height: 100,
          flexDirection: 'row',
          borderRadius: 20,
          elevation: 5,
          zIndex: 1,
          justifyContent: 'space-between',}}>
           
           <View style={{flexDirection:'column'}}>
            <Text>Penggalang Dana</Text>
            <View style={{flexDirection: 'row',padding:10}}>
                <Image source={{uri : 'https://crowdfunding.sanberdev.com' + data.user.photo}} style={{ width: 40,height: 40,borderRadius: 40}} />
                <Text style={{fontSize: 15,fontWeight: "bold",padding:7,marginLeft:5}}>{data.user.name}</Text>
            </View>
          </View>
           
        </View> 
{renderDonation()}
            </View>
        </View>
    )
}

export default DetailDonasi

const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      marginVertical: 250,
      marginHorizontal: 20
    },
    modalView: {
      height: 230,
      margin: 10,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 10,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    },

    // modal
    // item: {
        
    //   },
      title: {
        fontSize: 1,
      },
  });
