import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { WebView } from 'react-native-webview'

const Payment = ({route}) => {
    return (
        <WebView
        style={{flex:1}}
        source={{uri : route.params.midtrans.redirect_url }}
        />
    )
}

export default Payment

const styles = StyleSheet.create({})
