import React,{useState,useEffect,useRef} from 'react'
import { StyleSheet, Text, View,TouchableOpacity,Image,Alert,ToastAndroid,Modal } from 'react-native'
import colors from '../../style/StyleTugas6/colors'
import { Input,Button  } from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { TextInputMask } from 'react-native-masked-text'
import { RNCamera } from 'react-native-camera';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../api'

const BuatDonasi = () => {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [donation, setDonation] = useState('')
    const [tokens, setTokens] = useState('')
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)
    const [isVisible, setIsVisible] = useState(false)
    const camera = useRef(null)
    

    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("token")
                setTokens(token)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    }, [])

    const toggleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        const option = {quality: 0.5, base64 : true};
        if(camera){
            const data = await camera.current.takePictureAsync(option);
            setPhoto(data)
            setIsVisible(false)
        }
    }
    
    const header = {
        'Authorization' : 'Bearer ' + tokens,
        'Content-Type' :  'multipart/form-data'
    }

    const onBuatDonasiPress = () => {
    

        let donations =  donation.replace(/[^\w\s]/gi, '')
        let donations1 =  donations.replace('Rp', '')

        let formData = new FormData()
        formData.append('title',title)
        formData.append('description',description)
        formData.append('donation',donations1)
        if(photo === null){
            formData.append('photo','tidak ada photo')
        }else{
            formData.append('photo', {
                uri: photo.uri,
                name: 'photo.jpg',
                type: 'image/jpg'
            })
        }

        console.log(donations1)

        if(formData === null){
            Alert.alert('Peringatan',
            'Terjadi kesalahan/ error',
                [{
                    text : 'Ya',
                    onPress : () => console.log('Tidak')
                }])
        }else{
            Axios.post(`${api}/donasi/tambah-donasi`,formData,{
                timeout: 20000,
                headers : header 
            }).then((res) => {
                console.log("success -> res",res.data)
                Alert.alert('Selamat',
                    'Donasi Baru berhasil ditambahkan',
                        [{
                            text : 'Ya',
                            onPress : () => console.log('Tidak')
                        }])

            }).catch((err) => {
                Alert.alert('Peringatan',
                    'Terjadi kesalahan/ error',
                        [{
                            text : 'Ya',
                            onPress : () => console.log('Tidak')
                        }])
                console.log(err)
            })
        }
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={camera}
                    >
                        <View style={{
                             padding:25,
                             width:80,
                             height:80,
                             borderRadius:80,
                             backgroundColor:colors.blue
                        }}>
                            <TouchableOpacity style={{}} onPress={() => toggleCamera()}>
                                <MaterialCommunityIcons name={'rotate-3d-variant'} size={30} color={colors.white}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                              width:300,
                              height:500,
                              borderRadius:80,
                              borderColor:'black',
                              borderWidth:3,
                              borderColor: colors.black,
                              marginHorizontal: 50
                        }} />
                        <View style={{
                            padding:25,
                            width:80,
                            height:80,
                            borderRadius:80,
                            backgroundColor:colors.blue,
                            marginVertical: 30,
                            marginHorizontal: '40%'
                        }} >
                            <TouchableOpacity style={{}} onPress={() => takePicture()}>
                                <Feather name={'camera'} size={30} color={colors.white} />
                            </TouchableOpacity>
                        </View>

                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View>
            {
            photo == null ? 
            <View style={{width: '100%',height: '40%',backgroundColor:colors.grey}}>
                <TouchableOpacity style={{paddingTop: 10,alignItems:"center",marginVertical:90}} onPress={() => setIsVisible(true)}>
                <Feather name={'camera'} size={30} />
                <Text style={{textAlign:'center'}}>Pilih Gambar</Text> 
                </TouchableOpacity>
            </View> :
             <Image source={{uri : photo.uri}} style={{width: '100%',height: '40%'}} />
            }  

            <View style={{margin: 20,flexDirection:'column'}}>
                <Input
                value={title}
                onChangeText={(title) => setTitle(title)}
                placeholder='Judul'
                label='Judul'
                labelStyle={{color:'black',fontWeight: '100'}}
                />

            <Input
                value={description}
                onChangeText={(description) => setDescription(description)}
                placeholder='Deskripsi'
                label='Deskripsi'
                labelStyle={{color:'black',fontWeight: '100'}}
                required
                />

            <View style={{margin:10}}>
            <Text>Dana yang dibutuhkan</Text>
            <TextInputMask
            type={'money'}
            options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp. ',
                suffixUnit: ''
            }}
            value={donation}
            onChangeText={(donation) => setDonation(donation)}
            placeholder={'Rp. 0'}
            />
            <View
            style={{
                borderBottomColor: colors.grey,
                borderBottomWidth: 1,
            }}
            />
            </View>

            <Button
                title="BUAT"
                onPress={() => onBuatDonasiPress()}
                />
            </View>
            {renderCamera()}
        </View>
    )
}

export default BuatDonasi

const styles = StyleSheet.create({})
