import React,{useEffect,useState} from 'react'
import { StyleSheet, Text, View,SafeAreaView, FlatList, StatusBar  } from 'react-native'
import colors from '../../style/StyleTugas6/colors'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../api'

// const DATA = [
//     {
//       id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
//       title: 'First Item',
//     },
//     {
//       id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
//       title: 'Second Item',
//     },
//     {
//       id: '58694a0f-3da1-471f-bd96-145571e29d72',
//       title: 'Third Item',
//     },
//   ];

  

const Riwayat = () => {

    const [riwayat, setRiwayat] = useState([])

    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("token")
                return getRiwayat(token)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
        getRiwayat()
    }, [])

    const Item = ({ total,donasiId,tgl }) => (
        <View style={styles.item}>
          <Text style={styles.title1}>Total Rp. {total}</Text>
          <Text style={styles.title}>Donation ID: {donasiId}</Text>
          <Text style={styles.title}>Tanggal Transaksi: {tgl}</Text>
        </View>
      );

    const getRiwayat = (token) =>{
        Axios.get(`${api}/donasi/riwayat-transaksi`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer' + token,
                'Accept' : 'application/json',
                'Content-Type' :  'application/json'
            }
        }).then((res) =>{
            setRiwayat(res.data.data.riwayat_transaksi)
        }).catch((err) =>{
            console.log(err)
        })
    }

    const renderItem = ({ item }) => (
        <Item total={item.amount} donasiId={item.order_id}  tgl={item.created_at}/>
      );
    
      return (
        <SafeAreaView style={styles.container}>
          <FlatList
            data={riwayat}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
        </SafeAreaView>
      );
}

export default Riwayat

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item: {
        backgroundColor: colors.white,
        padding: 20,
        marginVertical: 3,
      },
      title1: {
        fontSize: 13,
        fontWeight: "bold"
      },
      title: {
        fontSize: 13,
      },
})
