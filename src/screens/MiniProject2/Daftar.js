import React,{useState} from 'react'
import { View, Text,SafeAreaView,StatusBar,Button,TextInput,Alert } from 'react-native'
import colors from '../../style/StyleTugas6/colors'
import styles from '../../style/StyleMiniProject2/styles'
import { Input } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import api from '../../api';
import Axios from 'axios';

const Daftar = ({ navigation,route }) => {
    const [email, setEmail] = useState('')
    const [name, setName] = useState('')

    // let data = {
    //     email : email,
    //     name : name
    // }

    const onDaftarPress = () => {
        let data = {
            email : email,
            name : name
        }

        Axios.post(`${api}/auth/register`,data,{
            timeout: 20000
        }).then((res) => {
            console.log("Login -> res",res.data)
            navigation.navigate('DaftarOTP',data)
        }).catch((err) => {
            Alert.alert('Peringatan',
                'Email anda sudah tertdaftar',
                    [{
                        text : 'Ya',
                        onPress : () => console.log('Tidak')
                    }])
            console.log(err)
        })
    }

    return (
        <SafeAreaView style={styles.containerLogin}>
        <View style={styles.containerLogin}>
            <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
            <View style={styles.textLogoContainer}>
                <Text style={styles.textLogoLogin}>Daftar CrowdFunding</Text>
            </View>
            <View style={styles.inputLogin}>
            <Text >Nomor Ponsel atau Email</Text>
            <Input value={email} onChangeText={(email) => setEmail(email)} placeholder='Nomor Ponsel atau Email'/>
            <Text >Nama Lengkap</Text>
            <Input value={name} onChangeText={(name) => setName(name)}  placeholder='Nama Lengkap'/>
            <Button title="Daftar" onPress={() => onDaftarPress()}/>
            {/* <Button title="Daftar" onPress={() => navigation.navigate('DaftarOTP',data)}/> */}
                <View style={styles.textLoginDaftar}>
                    <Text>Sudah Punya Akun? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={{color : colors.blue}} > Masuk</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </SafeAreaView>
    )
}

export default Daftar