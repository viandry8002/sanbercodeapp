import React,{useState} from 'react'
import { View, Text,SafeAreaView,StatusBar,Button,TextInput,Alert } from 'react-native'
import colors from '../../style/StyleTugas6/colors'
import styles from '../../style/StyleMiniProject2/styles'
import { Input } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import api from '../../api';
import Axios from 'axios';

const DaftarOTP = ({ navigation,route }) => {
    const [otp, setOtp] = useState('')

    // let data = {
    //     otp : otp,
    //     email : route.params.email,
    //     name : route.params.name
    // }

    const onDaftarPress = () => {
        let data1 = {
            otp : otp,
        }
        let data2 = {
            email : route.params.email,
            name : route.params.name
        }

        Axios.post(`${api}/auth/verification`,data1,{
            timeout: 20000
        }).then((res) => {
            console.log("Login -> res",res.data.response_code)
            if(res.data.response_code === '00'){
                navigation.navigate('DaftarKataSandi',data2)
                // console.log("veriv berhasil",res.data)
            }else{
                Alert.alert('Pemberitahuan',
                'Kode OTP Salah coba ketik kembali',
                    [{
                        text : 'Ya',
                        onPress : () => console.log('Tidak')
                    }])
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    const onRegenerateOTPPress = () => {
        let data = {
            email : route.params.email
        }

        Axios.post(`${api}/auth/regenerate-otp`,data,{
            timeout: 20000
        }).then((res) => {
            console.log("Login -> res",res.data)
            Alert.alert('Pemberitahuan',
            'Kode OTP Sudah di kirim kembali',
                [{
                    text : 'Ya',
                    onPress : () => console.log('Tidak')
                }])
        }).catch((err) => {
            console.log(err)
        })
    }

    return (
        <SafeAreaView style={styles.containerLogin}>
        <View style={styles.containerLogin}>
            <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
            <View style={styles.textOTPContainer}>
                <Text style={styles.textotp1}>Perjalanan Kebaikanmu dimulai dari sini</Text>
                <Text style={styles.textotp2}>Masukan 6 digit kode yang kami kirim ke </Text>
                <Text style={styles.textotp1}>{route.params.email}</Text>
            </View>
            <View style={styles.inputLogin}>
           
            <OTPInputView
                style={{width: '80%', height: 200,marginHorizontal:30,marginTop: -50}}
                pinCount={6}
                // code={otp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                // onCodeChanged = {otp => { this.setOtp({otp})}}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled = {(otp) => setOtp(otp)}
            />

            <Button title="VERIFIKASI" onPress={() => onDaftarPress()}/>
            {/* <Button title="VERIFIKASI" onPress={() => navigation.navigate('DaftarKataSandi',data)}/> */}
                <View style={styles.textLoginDaftar}>
                    <Text style={{color : colors.grey}}>Belum menerima kode verifikasi </Text>
                </View>
                <View style={styles.textLoginDaftar}>
                <TouchableOpacity onPress={() => onRegenerateOTPPress()}>
                    <Text style={{color : colors.blue,fontWeight:"bold"}} >Kirim Ulang</Text>
                    </TouchableOpacity>
                    </View>
            </View>
        </View>
    </SafeAreaView>
    )
}

export default DaftarOTP