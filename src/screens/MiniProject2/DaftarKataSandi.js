import React,{useState} from 'react'
import { View, Text,SafeAreaView,StatusBar,Button,TextInput,Alert } from 'react-native'
import colors from '../../style/StyleTugas6/colors'
import styles from '../../style/StyleMiniProject2/styles'
import { Input } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import Axios from 'axios';
import api from '../../api';

const DaftarKataSandi = ({ navigation,route }) => {
    const [password, setPassword] = useState('')
    const [passwordConfirm, setPasswordConfirm] = useState('')


    console.log(route.params)

    const [isShow1, setIsShow1] = useState(true)
    const [isShow2, setIsShow2] = useState(true)

    const onButtonToggle1 = () => {
        setIsShow1(isShow1 === true ? false : true);
      };
    const onButtonToggle2 = () => {
        setIsShow2(isShow2 === true ? false : true);
      };

      const onDaftarPress = () => {
        let data = {
            // otp : otp,
            email : route.params.email,
            password : password,
            password_confirmation : passwordConfirm
        }

        Axios.post(`${api}/auth/update-password`,data,{
            timeout: 20000
        }).then((res) => {
            console.log("Login -> res",res.data)
            navigation.navigate('Login')
        }).catch((err) => {
            console.log(err)
        })
    }


    const validateDaftar = () => {
        if(password === passwordConfirm){
            console.log("berhasil input")
            onDaftarPress()
        }else{
            Alert.alert('Peringatan',
            'Password tidak sama silahkan input ulang',
                [{
                    text : 'Ya',
                    onPress : () => console.log('Tidak')
                }])
        }
    }

    return (
        <SafeAreaView style={styles.containerLogin}>
        <View style={styles.containerLogin}>
            <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
            <View style={styles.textLogoContainer}>
                <Text style={styles.textLogoLogin}>Tentukan Kata sandi baru untuk keamana akun kamu</Text>
            </View>
            <View style={styles.inputLogin}>
            <Input value={password} onChangeText={(password) => setPassword(password)} secureTextEntry={isShow1} placeholder='Password'  
                rightIcon={<Icon
                name='eye-off'
                size={24}
                color={colors.grey}
                onPress={() => onButtonToggle1()}/>}/>
            <Input value={passwordConfirm} onChangeText={(passwordConfirm) => setPasswordConfirm(passwordConfirm)} secureTextEntry={isShow2} placeholder='Konfirmasi Password'
            rightIcon={<Icon
                name='eye-off'
                size={24}
                color={colors.grey}
                onPress={() => onButtonToggle2()}/>}/>
            <Button title="Simpan" onPress={() => validateDaftar()}/>
            </View>
        </View>
    </SafeAreaView>
    )
}

export default DaftarKataSandi