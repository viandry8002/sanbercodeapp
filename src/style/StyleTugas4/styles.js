import React from 'react';
import { StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    judul:{
        padding:10
    },
    form:{
        flexDirection: 'row',
        padding:10
    },
    input:{
        width: 310,
        height: 50,
        borderWidth: 2,
        padding:10
    },
    button:{
        width: 50,
        height: 50,
        padding: 15,
        backgroundColor: '#40b2ff',
        marginHorizontal: 10
    },
    containerList:{
        margin: 10,
        padding: 5,
        flexDirection: 'row',
        borderWidth: 3,
        borderColor: '#b0b0b0'
    },
    textList:{
        flexDirection: 'column',
        padding: 5,
        flex : 11,
    },
    deleteList:{
        padding: 5,
        flex : 1
    }
})

export default styles
