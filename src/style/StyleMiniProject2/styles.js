import React from 'react'
import { StyleSheet } from 'react-native'
import colors from '../StyleTugas6/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blue
    },
    containerLogin: {
        flex: 1,
        backgroundColor: colors.white,
        paddingHorizontal: 5
    },
    textLogoContainer: {
        marginTop: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textLogo: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.white
    },
    textLogoLogin: {
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.black,
        marginBottom: 30
    },
    slider: {
        flex: 1
    },
    btnContainer: {
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    btnLogin: {
        height: 35,
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white
    },
    btnTextLogin: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.blue
    },
    btnRegister: {
        height: 35,
        width: '90%',
        borderWidth: 1.5,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors.white,
        backgroundColor: 'transparent'
    },
    btnTextRegister: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.white
    },
    listContainer: {
        marginTop: 25,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    listContent: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgList: {
        width: 330,
        height: 330
    },
    textList: {
        marginTop:10,
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.white
    },
    activeDotStyle: {
        width: 20,
        backgroundColor: colors.white
    },
    inputLogin : {
        flexDirection: 'column'
    },
    textLoginDaftar : {
        flexDirection: 'row',
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 30,
    },
    textLoginFinger : {
        marginTop: 30,
        marginHorizontal : 30
    },
    textOTPContainer: {
        paddingTop: 20,
    },
    textotp1:{
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.black,
        marginBottom: 20
    },
    textotp2:{
        fontSize: 15,
        marginBottom: 20,
    },
    // verifikasi otp
    borderStyleBase: {
        width: 30,
        height: 45
      },
    
      borderStyleHighLighted: {
        borderColor: "#03DAC6",
      },
    
      underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color:"black"
      },
    
      underlineStyleHighLighted: {
        borderColor: "#03DAC6"
      }
      // verifikasi otp
})

export default styles