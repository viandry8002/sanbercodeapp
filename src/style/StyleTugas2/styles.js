import React from 'react'
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
 
    avatar: {
      width: 50,
      height: 50,
      borderRadius: 50
  },
    container: {
      flex: 1
    },
    item: {
      flexDirection: 'row',
      backgroundColor: "#fff",
      padding: 15,
      marginVertical: 1,
    },
    item1: {
      flexDirection: 'row',
      backgroundColor: "#fff",
      padding: 20,
      marginVertical: 1,
    },
    item2: {
      flexDirection: 'row',
      backgroundColor: "#fff",
      padding: 15,
      marginBottom: 5,
    },
    title: {
      fontSize: 15,
      padding: 5
    },
    saldo: {
      fontSize: 15,
      padding: 5,
      marginLeft:125
    },
      desc: {marginLeft: 18, flex: 1, flexDirection: 'row'},
      descNama: {paddingTop: 12,fontSize: 17, fontWeight: 'bold'},
  });
  
  export default styles